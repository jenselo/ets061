package task2;

import java.io.IOException;

public class Task2 extends GlobalSimulation {
 
    //public static void main(String[] args) throws IOException {
	public static void run() {
		int k = 0;
		boolean[] expDel = {false, true};
		double[] res = new double[2];

		for(boolean b : expDel){
			// Reset simulation variables
			discards = 0;
			Event actEvent;
			State actState = new State(); // The state that should be used
			time = 0;

			// Some events must be put in the event list at the beginning
			insertEvent(ARRIVALQA, 0);
			insertEvent(MEASURE, 0.1);

			// The main simulation loop
			while (actState.noMeasurements < 1000) {
				actEvent = eventList.fetchEvent();
				time = actEvent.eventTime;
				actState.treatEvent(actEvent);
			}

			double meanNoCustomers = actState.accumulated / (double) actState.noMeasurements;
			res[k] = meanNoCustomers;
			k++;
		}

		System.out.println();
		System.out.println("Mean number of customers with constant delay: " + res[0]);
		System.out.println("Mean number of customers with exponentially dist. delay: " + res[1]);
	}
}
