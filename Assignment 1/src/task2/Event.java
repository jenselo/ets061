package task2;
// As the name indicates this class contains the definition of an event. next is needed to 
// build a linked list which is used by the task1.EventListClass. It would have been just as easy
// to use a priority list which sorts events using eventTime.

class Event{
	public double eventTime;
	public int eventType;
	public Event next;

	public String getEvent() {
		switch (eventType) {
			case GlobalSimulation.ARRIVALQA:
				return "ARRIVALQA";
			case GlobalSimulation.ARRIVALQB:
				return "ARRIVALQB";
			case GlobalSimulation.READYQA:
				return "READYQA";
			case GlobalSimulation.READYQB:
				return "READYQB";
			case GlobalSimulation.MEASURE:
				return "MEASURE";
			default:
				return "UNDEFINED";
		}
	}
}
