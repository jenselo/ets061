package task2;

class Signal{
	public Proc destination;
	public double arrivalTime;
	public double arrivalTimeToServer;
	public int signalType;
	public Signal next;
}
