package task1;

import java.util.*;
import java.io.*;

public class MainSimulation extends Global{

	public static void run() {
		int[] queueList = {RANDOM, ROUNDROBIN, SMART};
		double[] meanArrivalTimes = {0.12, 0.11, 0.15, 2.0};

		for(int ii = 0; ii < meanArrivalTimes.length; ii++) {
			for(int jj=0; jj < queueList.length; jj++) {
				int tempQueueType = queueList[jj];
				Costumer.reset();
				Signal actSignal;
				new SignalList();
				Global.time = 0;

				ArrayList<QS> QSS = new ArrayList<QS>();
				QS temp;
				for (int j = 0; j < 5; j++) {
					temp = new QS(j);
					temp.sendTo = null;
					QSS.add(temp);
				}

				Gen Generator = new Gen(QSS, tempQueueType, meanArrivalTimes[ii]);

				Generator.lambda = 9;
				Generator.sendTo = QSS.get(0);

				SignalList.SendSignal(READY, Generator, time);
				SignalList.SendSignal(MEASURE, QSS.get(0), time);
				SignalList.SendSignal(MEASURE, QSS.get(1), time);
				SignalList.SendSignal(MEASURE, QSS.get(2), time);
				SignalList.SendSignal(MEASURE, QSS.get(3), time);
				SignalList.SendSignal(MEASURE, QSS.get(4), time);

				while (time < 100000) {
					actSignal = SignalList.FetchSignal();
					time = actSignal.arrivalTime;
					actSignal.destination.TreatSignal(actSignal);
				}

				double[] meanCusts = new double[5];
				System.out.println("Meantime i system " + Costumer.getMean());

				meanCusts[0] = 1.0 * QSS.get(0).accumulated / QSS.get(0).noMeasurements;

				for(int i = 1; i < QSS.size(); i++) {
					meanCusts[i] = 1.0 * QSS.get(i).accumulated / QSS.get(i).noMeasurements;
				}
			}
		}
	}
}