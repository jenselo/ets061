package task4;
// As the name indicates this class contains the definition of an event. next is needed to 
// build a linked list which is used by the task1.EventListClass. It would have been just as easy
// to use a priority list which sorts events using eventTime.

class Event{
	public double eventTime;
	public int eventType;
	public Event next;

	public String getEvent() {
		switch (eventType) {
			case GlobalSimulation.ARRIVAL:
				return "ARRIVAL";
			case GlobalSimulation.READY:
				return "READY";
			case GlobalSimulation.MEASURE:
				return "MEASURE";
			default:
				return "UNDEFINED";
		}
	}
}
