package task3alt;

import java.util.LinkedList;
import java.util.PriorityQueue;

/**
 * Created by Per on 31/05/2014.
 */
public class QS extends Proc {
	private static int measurment;
	private static int totalInQeueu;
	LinkedList<Customer> queue;
	public QS(){
		queue = new LinkedList<Customer>();
	}

	public void treatSignal(Signal x){
		switch (x.getType()) {
			case Signal.ARRIVAL:
					queue.add(x.getCustomer());
					if(queue.size()==1){
						MainSimulation.signalList.add(new Signal(this,Signal.READY,x.getArivallTime()+MainSimulation.getExponatial(1),null));
					}
					break;
			case Signal.READY:
				Customer c = queue.poll();
				if(next!=null){
					MainSimulation.signalList.add(new Signal(next,Signal.ARRIVAL,x.getArivallTime(),c));
				}
				if(queue.size()>0){
					MainSimulation.signalList.add(new Signal(this,Signal.READY,x.getArivallTime()+MainSimulation.getExponatial(1),null));
				}
				if(next == null){
					c.killCostumer(x.getArivallTime());
				}
				break;
			case Signal.MEASUREMENT:
				measurment ++;
				//System.out.println(measurment);
				totalInQeueu += queue.size() + ((QS) next).getSize();
				MainSimulation.signalList.add(new Signal(this,Signal.MEASUREMENT,x.getArivallTime()+MainSimulation.getExponatial(5.0),null));
				break;
		}
	}
	public static int returnMeasur(){
		return measurment;
	}
	public static void reset(){
		measurment = totalInQeueu=0;
	}
	public int getSize(){
		return queue.size();
	}
	public static double getMeanNumberInQueue(){
		return (double)totalInQeueu/(double)measurment;
	}
	

}
