package task1;
import java.util.Random;

public class Global {
	public static final int ARRIVAL = 1, READY = 2, MEASURE = 3;
	public static final int RANDOM = 1, ROUNDROBIN = 2, SMART=3;
	public static double time = 0;
	public static int queueType = 0;
}
