package task2_rev.task2_rev;
// As the name indicates this class contains the definition of an event. next is needed to 
// build a linked list which is used by the task1.EventListClass. It would have been just as easy
// to use a priority list which sorts events using eventTime.

class Event{
	public double eventTime;
	public int eventType;
	public Event next;

	public Event(int eventType, double eventTime) {
		this.eventType = eventType;
		this.eventTime = eventTime;
	}

	public Event() {};

	public double getEventTime() {
		return eventTime;
	}

	public String getEventType() {
		switch (eventType) {
			case GlobalSimulation.ARRIVAL:
				return "ARRIVAL";
			case GlobalSimulation.READY_ARRIVAL:
				return "READY_ARRIVAL";
			case GlobalSimulation.TEARDOWN:
				return "TEARDOWN";
			case GlobalSimulation.READY_TEAR_DOWN:
				return "READY_TEARDOWN";
			case GlobalSimulation.MEASURE:
				return "MEASURE";
			default:
				return "UNDEFINED";
		}
	}
}
