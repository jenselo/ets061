package task2;

import java.util.Random;
import java.util.ArrayList;

class QS extends Proc {
	public int numberInQueue = 0, accumulated, noMeasurements;
	public Proc sendTo;
	Random slump = new Random();
	public int name;
	public int numberOfRecipes = 0;
	public double totalTimeRecipes = 0;
	public double totalQueueTime = 0;
	private ArrayList<Double> startTimes;

	public QS(int name){
		this.name = name;
		startTimes = new ArrayList<Double>();
		this.totalQueueTime = 0;
	}

	public void TreatSignal(Signal x){
		switch (x.signalType){
			case ARRIVAL:{
				numberOfRecipes++;
				numberInQueue++;
				double tempTime = time;

				if (numberInQueue == 1){
					double serviceTime = 10*slump.nextDouble()+10;
					Signal temp = SignalList.SendSignal(READY, this, time + serviceTime);
					temp.arrivalTimeToServer = time;
					tempTime += serviceTime;
				}

				startTimes.add(tempTime);
				break;
			}

			case READY:{
				numberInQueue--;
				//totalTimeRecipes +=  x.arrivalTime -x.arrivalTimeToServer;
				totalTimeRecipes += time - x.arrivalTimeToServer;
				totalQueueTime += time + startTimes.remove(0);

				if (sendTo != null){
					SignalList.SendSignal(ARRIVAL, sendTo, time);
				}
				if (numberInQueue > 0){
					Signal temp = SignalList.SendSignal(READY, this, time + 10 * slump.nextDouble()+10);
					temp.arrivalTimeToServer = time;
				}
				break;
			}

			case MEASURE:{
				noMeasurements++;
				accumulated = accumulated + numberInQueue;
				SignalList.SendSignal(MEASURE, this, time + 2 * slump.nextDouble());
				break;
			}
		}
	}

	public void resetTimeList() {
		startTimes = new ArrayList<Double>();
	}
}