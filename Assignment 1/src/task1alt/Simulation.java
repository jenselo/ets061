package task1alt;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Random;

/**
 * Created by Per on 29/05/2014.
 */
public class Simulation {
	public static PriorityQueue<Event> eventQueue = new PriorityQueue<Event>();
	public static Random rand = new Random();
	public static ArrayList<Queuer> q1 = new ArrayList<Queuer>();
	public static ArrayList<Queuer> q2 = new ArrayList<Queuer>();
	public static double arriveTime1 = 0;


	public static void run(){
		arriveTime1 =1;
		simulate();
		arriveTime1=2;
		simulate();
		arriveTime1=5;
		simulate();
	}

	public static void simulate(){
		System.out.println("Mean arrival time: " + arriveTime1);
		Measurements.reset();
		EventQueue1.reset();
		q1 = new ArrayList<Queuer>();
		q2 = new ArrayList<Queuer>();
		eventQueue = new PriorityQueue<Event>();
		eventQueue.add(new EventQueue1(0,q1));
		eventQueue.add(new Measurements(-(Math.log(Simulation.rand.nextDouble()) / (1/5.0))));

		while(Measurements.getMeasurement()<100000){
			Event t = eventQueue.poll();
			t.treatEvent();
		}

		System.out.println("\tMean time in queue: " + Measurements.getTotalInQueue()/100000.0);
		System.out.println("\tChance of rejection: " + EventQueue1.chanceOfRejection());
	}

}
