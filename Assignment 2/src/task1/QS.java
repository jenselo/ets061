package task1;

import java.util.*;

class QS extends Proc{
	public int numberInQueue = 0, accumulated, noMeasurements;
	private LinkedList<Costumer> queue;
	public Proc sendTo;
	Random slump = new Random();
	public int name;
	public QS(int name){
		this.name = name;
		queue = new LinkedList<Costumer>();
	}

	public void TreatSignal(Signal x){
		switch (x.signalType){

			case ARRIVAL:{
				queue.add(new Costumer(time));
				if (queue.size() == 1){
					double meanServiceTime = 0.5;
					double serviceTime = -Math.log(slump.nextDouble()) / (1/meanServiceTime);
					SignalList.SendSignal(READY,this, time + serviceTime);
				}
			} break;

			case READY:{
				Costumer temp =queue.poll();
				temp.getCalculateTime(time);

				if (sendTo != null){
					SignalList.SendSignal(ARRIVAL, sendTo, time);
				}
				if (queue.size() > 0){
					SignalList.SendSignal(READY, this, time + 0.2*slump.nextDouble());
				}
			} break;

			case MEASURE:{
				noMeasurements++;
				accumulated = accumulated + queue.size();
				SignalList.SendSignal(MEASURE, this, time + 0.1);
			} break;
		}
	}

	public int getSize() {
		return queue.size();
	}
}