package task4;

import java.io.FileWriter;

public class GlobalSimulation{
	
	// This class contains the definition of the events that shall take place in the
	// simulation. It also contains the global time, the event list and also a method
	// for insertion of events in the event list. That is just for making the code in
	// task1.Task4.java and task1.State.java simpler (no dot notation is needed).
	
	public static final int ARRIVAL = 1, READY = 2, MEASURE = 3; // The events, add or remove if needed!
	public static double time = 0; // The global time variable

	// Parameters from the assignment (gets set in Task4.java)
	public static int N = 0;
	public static int x = 0;
	public static int lambda = 0;
	public static int T = 0;
	public static int M = 0;

	public static FileWriter fw = null;

	public static EventListClass eventList = new EventListClass(); // The event list used in the program
	public static void insertEvent(int type, double TimeOfEvent){  // Just to be able to skip dot notation
		eventList.InsertEvent(type, TimeOfEvent);
	}

	public static void reset() {
		time = 0;
		eventList = new EventListClass();
	}
}