package task2Alternativ;

import java.io.IOException;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Random;
import java.io.PrintWriter;

public class Simulation2 {
	public static PriorityQueue<Event> eventQueue = new PriorityQueue<Event>();
	public static Random rand = new Random();
	public static double arriveTime1 = 2;
	public static final int RUNTIME =1000;
	private static int i;

	public static void run() throws IOException{
		DelayEvent.SetCONST();
		Buffer b = new Buffer(Buffer.B);
		simulate(b);
		i++;
		DelayEvent.setEXP();
		b =new Buffer(Buffer.B);
		simulate(b);
		i++;
		DelayEvent.SetCONST();
		b = new Buffer(Buffer.A);
		simulate(b);
	}

	public static void simulate(Buffer b)throws IOException{

		Measurements.reset();
		eventQueue = new PriorityQueue<Event>();
		eventQueue.add(new ArrivalEvent(0,b));
		eventQueue.add(new Measurements(0.1,b));
		while (Measurements.getNumberMeasurment()<RUNTIME) {
			eventQueue.poll().treatEvent();
		}

		System.out.println("Mean number of jobs in system: " + Measurements.meanNumbrJobs());

			PrintWriter writer = new PrintWriter("OUT"+new Integer(i).toString()+".txt","UTF-8");
			for(int k =0;k< Measurements.measur.size();k++){
				writer.print(Measurements.measur.get(k)+" ");
			}

			writer.close();



	}
	public static double getPoisson(double arrivalrate){
		return (-1.0/arrivalrate)*Math.log(rand.nextDouble());
	}
	public static double getExponatial(double mean){
		return -(Math.log(rand.nextDouble()) / (1.0/mean));
	}



}
