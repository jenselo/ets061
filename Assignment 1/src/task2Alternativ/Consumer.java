package task2Alternativ;

import java.lang.Integer;

public class Consumer implements Comparable<Consumer>{
	public static int A=1,B=0;
	private int number,type;
	private double time;

	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Consumer consumer = (Consumer) o;

		if (Double.compare(consumer.time, time) != 0) return false;
		if (type != consumer.type) return false;

		return true;
	}


	public int compareTo(Consumer o) {
		Integer p = new Integer(number);

		return p.compareTo(o.number);
	}
	public String toString(){
		return "Consumer "+type;
	}

	public Consumer(int type,double time){
		this.type = type;
		this.time = time;
		number +=1;
	}
	public int getType(){
		return type;
	}
	public double getTime(){
		return time;
	}
	public static void bPrio(){
		B = 0;
		A = 1;
	}
	public static void aPrio(){
		A = 0;
		B = 1;
	}

	public static void reset(){

	}
}