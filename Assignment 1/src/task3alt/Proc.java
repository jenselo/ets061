package task3alt;

//import com.sun.scenario.effect.impl.prism.PrCropPeer;

/**
 * Created by Per on 31/05/2014.
 */
public abstract class Proc extends Global {
	Proc next ;

	public abstract void treatSignal(Signal x);
	public void setNextProc(Proc proc){
		next = proc;
	}
}
