package task3;

import java.util.*;

class State extends GlobalSimulation{
	// Here follows the state variables and other variables that might be needed
	// e.g. for measurements
	public int numberInQueue1 = 0, numberInQueue2 = 0, accumulatedCusts = 0, noMeasurements = 0;
	public double accumulatedTime = 0.0;
	private double meanServiceTime1 = 1.0;
	private double meanServiceTime2 = 1.0;
	private double meanInterArrivalTime = 0.0;
	private int noCusts = 0;

	private ArrayList<Double> startTimes = new ArrayList<Double>();
	private ArrayList<Double> stopTimes = new ArrayList<Double>();
	private Random chance = new Random();

	public State(double meanInterArrivalTime) {
		this.meanInterArrivalTime = meanInterArrivalTime;
		GlobalSimulation.time = 0.0;
	}

	// The following method is called by the main program each time a new event has been fetched
	// from the event list in the main loop. 
	public void treatEvent(Event x){
		switch (x.eventType){
			case ARRIVALQ1:
				arrivalQ1();
				break;
			case READYQ1:
				readyQ1();
				break;
            case ARRIVALQ2:
                arrivalQ2();
				break;
            case READYQ2:
                readyQ2();
				break;
			case MEASURE:
				measure();
				break;
		}
	}
	
	
	// The following methods defines what should be done when an event takes place. This could
	// have been placed in the case in treatEvent, but often it is simpler to write a method if 
	// things are getting more complicated than this.
	
	private void arrivalQ1(){
		startTimes.add(time);
		if(numberInQueue1 == 0) {
			double serviceTime1 = -(Math.log(chance.nextDouble()) / (1/meanServiceTime1));
			insertEvent(READYQ1, time+serviceTime1);
		}

		double interArrivalTime = -(Math.log(chance.nextDouble()) / (1/meanInterArrivalTime));
		numberInQueue1++;
		insertEvent(ARRIVALQ1, time+interArrivalTime);
	}

    private void arrivalQ2() {
		if(numberInQueue2 == 0) {
			double serviceTime2 = -(Math.log(chance.nextDouble()) / (1/meanServiceTime2));
			insertEvent(READYQ2, time+serviceTime2);
		}

		numberInQueue2++;
    }
	
	private void readyQ1() {
		if(numberInQueue1 > 0) {
			numberInQueue1--;

			insertEvent(ARRIVALQ2, time + 0); // Schedule it to now

			// Process next customer
			double serviceTime1 = -(Math.log(chance.nextDouble()) / (1 / meanServiceTime1));
			//insertEvent(READYQ1, time+serviceTime1);
		}
	}

    private void readyQ2() {
		if(numberInQueue2 > 0) {
			numberInQueue2--;

			stopTimes.add(time);

			double start = startTimes.remove(0);
			double stop = stopTimes.remove(0);

			double timeSpent = stop - start;

			accumulatedTime += timeSpent;
			noCusts++;

			// Process next customer
			double serviceTime2 = -(Math.log(chance.nextDouble()) / (1 / meanServiceTime2));
			insertEvent(READYQ2, time + serviceTime2);
		}
    }
	
	private void measure() {
		accumulatedCusts += numberInQueue1 + numberInQueue2;

		double meanMeasureInterval = -(Math.log(chance.nextDouble()) / (1/5.0));
		insertEvent(MEASURE, time+1.0);
		noMeasurements++;
	}

	public double getMeanTimeSpent() {
		return accumulatedTime/noCusts;
	}

	public double getMeanCustomers() {
		return accumulatedCusts/(double)noMeasurements;
	}

	public int getCustomers() {
		return accumulatedCusts;
	}

	public int getCustomersNonAcc() {
		return noCusts;
	}

	public int getNoMeasurements() {
		return noMeasurements;
	}
}