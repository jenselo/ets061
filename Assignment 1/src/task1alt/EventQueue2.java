package task1alt;

import java.util.ArrayList;

/**
 * Created by Per on 29/05/2014.
 */
public class EventQueue2 extends EventQueue {

	public EventQueue2(double time,ArrayList<Queuer> queue){
		super(time,queue);
	}

	public void treatEvent() {
		if(queue.size()==0){
			Simulation.eventQueue.add(new EventReady2(time+2.0,queue));
		}
		increaseQueue();

	}


	protected void increaseQueue() {
		queue.add(new Queuer());

	}
}
