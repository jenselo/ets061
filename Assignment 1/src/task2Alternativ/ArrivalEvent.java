package task2Alternativ;

import task1alt.*;

import java.util.ArrayList;
import java.util.PriorityQueue;

/**
 * Created by Per on 29/05/2014.
 */
public class ArrivalEvent extends Event {
	Buffer queue;
	public ArrivalEvent(double time,Buffer queue){
		super(time);
		this.queue = queue;
	}
	public void treatEvent() {
		Consumer c = new Consumer(Consumer.A,time);
		queue.addBufferA(c);
		if(queue.size()==1){
			Simulation2.eventQueue.add(new ReadyEvent(time+ReadyEvent.Xa,queue,queue.chooser()));
		}
		Simulation2.eventQueue.add(new ArrivalEvent(time+Simulation2.getPoisson(150),queue));

	}
}
