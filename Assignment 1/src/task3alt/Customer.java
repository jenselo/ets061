package task3alt;

/**
 * Created by Per on 31/05/2014.
 */
public class Customer {
	private double startTime;
	private static double totalTime;
	private static int numCost;
	public Customer(double startTime){
		this.startTime = startTime;
		numCost++;
	}
	public void killCostumer(double time){
		totalTime += time -startTime;
	}
	public static void reset(){
		totalTime=numCost=0;
	}
	public static double meanTimeInSystem(){
		return totalTime/(double)numCost;
	}

}
