package task1alt;

import java.util.ArrayList;
import java.util.Queue;

/**
 * Created by Per on 29/05/2014.
 */
public abstract class  EventQueue extends Event {
	//static private int queue;
	protected ArrayList<Queuer> queue;
	public EventQueue(double time,ArrayList<Queuer> queue)
	{
		super(time);
		this.queue = queue;
	}
	abstract protected void increaseQueue();
	public int getQueueSize(){
		return queue.size();
	}
	//public abstract void treatEvent();

}
