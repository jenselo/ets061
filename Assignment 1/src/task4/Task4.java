package task4;

import java.io.*;

public class Task4 extends GlobalSimulation {

//public static void main(String[] args) throws IOException {
	public static void run() {
		int[] Ns = {1000, 1000, 1000, 100, 100, 100};
		int[] xs = {100, 10, 200, 10, 10, 10};
		int[] lambdas = {8, 80, 4, 4, 4, 4};
		int[] Ts = {1, 1, 1, 4, 1, 4};
		int[] Ms = {1000, 1000, 1000, 1000, 4000, 4000};


		// Simulation loop for tasks 1:6
		for(int i = 0; i < Ns.length; i++) {
			System.out.println("Simulating task " + (i+1) + "...");
			System.out.println(T);
			N = Ns[i];
			x = xs[i];
			lambda = lambdas[i];
			T = Ts[i];
			M = Ms[i];

			Event actEvent;
			State actState = new State(); // The state that should be used
			GlobalSimulation.reset();

			// Some events must be put in the event list at the beginning
			insertEvent(ARRIVAL, 0);
			insertEvent(MEASURE, time + T);

			// The main simulation loop

			// Open file
			String fName = null;
			try {
				fName = "task" + (i+1) + ".txt";
				fw = new FileWriter(fName);

				// Write parameters to file
				String text = N + "," + x + "," + lambda + "," + T + "," + M;
				fw.write(text + '\n');
			} catch (IOException e) {
				System.out.println("ERROR: Couldn't open " + fName);
				System.exit(1);
			}


			while (actState.noMeasurements < M) {
				actEvent = eventList.fetchEvent();
				time = actEvent.eventTime;
				actState.treatEvent(actEvent);
			}

			//Close file
			try {
				fw.close();
			} catch (IOException e) {
				System.out.println("ERROR: Couldn't close output.txt.");
			}
		}
	}

}
