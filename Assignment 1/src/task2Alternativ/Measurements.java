package task2Alternativ;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.PriorityQueue;

/**
 * Created by Per on 30/05/2014.
 */
public class Measurements  extends Event {
	private static int totalJobs,numberMeasurments;
	public static ArrayList<Integer> measur = new ArrayList<Integer>();
	private Buffer queue;
	public Measurements(double time ,Buffer queue){
		super(time);
		this.queue = queue;

	}

	public void treatEvent() {
		//System.out.println(queue.size());
		int temp = queue.size();
		totalJobs +=temp; // queue.size();
		//System.out.println(totalJobs);
		measur.add(temp);
		numberMeasurments++;
		Simulation2.eventQueue.add(new Measurements(time+0.1,queue));

	}
	public static double meanNumbrJobs(){
		return totalJobs/(double)numberMeasurments;
	}
	public static void reset(){
		totalJobs=numberMeasurments=0;
		measur = new ArrayList<Integer>();
	}
	public static int getNumberMeasurment(){
		return numberMeasurments;
	}
}
