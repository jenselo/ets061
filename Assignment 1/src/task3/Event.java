package task3;
// As the name indicates this class contains the definition of an event. next is needed to 
// build a linked list which is used by the task1.EventListClass. It would have been just as easy
// to use a priority list which sorts events using eventTime.

class Event{
	public double eventTime;
	public int eventType;
	public Event next;

	public String getEvent() {
		switch (eventType) {
			case GlobalSimulation.ARRIVALQ1:
				return "ARRIVALQ1";
			case GlobalSimulation.ARRIVALQ2:
				return "ARRIVALQ2";
			case GlobalSimulation.READYQ1:
				return "READYQ1";
			case GlobalSimulation.READYQ2:
				return "READYQ2";
			case GlobalSimulation.MEASURE:
				return "MEASURE";
			default:
				return "UNDEFINED";
		}
	}
}
