package task2Alternativ;

import task1alt.Simulation;

import java.util.PriorityQueue;
import java.util.Random;

/**
 * Created by Per on 29/05/2014.
 */
public class ReadyEvent extends Event {
	private Buffer queue;
	private int typ;
	public static final double Xa =0.002;
	public static final double Xb =0.004;


	public ReadyEvent(double time,Buffer queue,int type){
		super(time);
		this.queue = queue;
		typ = type;


	}

	public void treatEvent() {
		queue.pull(typ);
		if(queue.size()>0){
			int temp = queue.chooser();
			Simulation2.eventQueue.add(new ReadyEvent(time+getTime(temp),queue,temp));
		}
		if(typ==Buffer.A){
			Simulation2.eventQueue.add(new DelayEvent(time+DelayEvent.timeToAdd(),queue));
		}


	}
	public static double getTime(int typ){
		return typ == Buffer.TYPA?Xa:Xb;
	}

}
