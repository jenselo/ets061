package task1alt;

import java.util.ArrayList;

/**
 * Created by Per on 29/05/2014.
 */
public class EventReady2 extends EventReady {
	public EventReady2(double time, ArrayList<Queuer> queue){
		super(time,queue);
	}


	public void treatEvent() {
		//System.out.println("Queue 2"+((Object)queue).toString());
		if(queue.size()>1){
			Simulation.eventQueue.add(new EventReady2(time +2.0 , queue));
		}
		decreaseQueue();
	}


	protected void decreaseQueue() {
		queue.remove(queue.size()-1);
	}
}
