package task1;

import java.util.*;

class State extends GlobalSimulation{
	// Here follows the state variables and other variables that might be needed
	// e.g. for measurements
	public int numberInQueue1 = 0, numberInQueue2 = 0, accumulatedQ1 = 0, accumulatedQ2 = 0, noMeasurements = 0;

	Random chance = new Random(); // This is just a random number generator
	
	
	// The following method is called by the main program each time a new event has been fetched
	// from the event list in the main loop. 
	public void treatEvent(Event x){
		switch (x.eventType){
			case ARRIVALQ1:
				arrivalQ1();
				break;
			case READYQ1:
				readyQ1();
				break;
            case ARRIVALQ2:
                arrivalQ2();
				break;
            case READYQ2:
                readyQ2();
				break;
			case MEASURE:
				measure();
				break;
		}
	}
	
	
	// The following methods defines what should be done when an event takes place. This could
	// have been placed in the case in treatEvent, but often it is simpler to write a method if 
	// things are getting more complicated than this.
	
	private void arrivalQ1(){
		if (numberInQueue1 == 0) {
			double nextTime = -(Math.log(chance.nextDouble()) / (1/2.1));
			insertEvent(READYQ1, time + nextTime);
			numberInQueue1++;
		} else if(numberInQueue1 < 10) {
			numberInQueue1++;
		} else if(numberInQueue1 >= 10) {
			discards++;

		}
		insertEvent(ARRIVALQ1, time + arrivalTime);
	}

    private void arrivalQ2() {
        if(numberInQueue2 == 0) {
			insertEvent(READYQ2, time+2);
		}
        numberInQueue2++;
    }
	
	private void readyQ1() {
		if (numberInQueue1 > 0) {
			double tempTime = -(Math.log(chance.nextDouble()) / (1/2.1));
			insertEvent(READYQ1, time + tempTime);
			numberInQueue1--;
		}
        arrivalQ2();
	}

    private void readyQ2() {
		if(numberInQueue2 > 0) {
			insertEvent(READYQ2, time + 2);
			numberInQueue2--;
		}
    }
	
	private void measure(){
		accumulatedQ1 = accumulatedQ1 + numberInQueue1;
		accumulatedQ2 = accumulatedQ2 + numberInQueue2;
		noMeasurements++;
		Random chance = new Random();
		double tempTime = -(Math.log(chance.nextDouble()) / (1/5.0));
		insertEvent(MEASURE, time + tempTime);
	}
}