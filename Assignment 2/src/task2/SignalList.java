package task2;

public class SignalList{
	private  static Signal list, last;
	private  static int size;
	SignalList(){
    	list = new Signal();
    	last = new Signal();
		size = 0;
    	list.next = last;
	}

	public static Signal SendSignal(int type, Proc dest, double arrtime){
 	Signal dummy, predummy;
 	Signal newSignal = new Signal();
 	newSignal.signalType = type;
 	newSignal.destination = dest;
 	newSignal.arrivalTime = arrtime;
 	predummy = list;
 	dummy = list.next;
 	while ((dummy.arrivalTime < newSignal.arrivalTime) & (dummy != last)){
 		predummy = dummy;
 		dummy = dummy.next;
 	}
 	predummy.next = newSignal;
 	newSignal.next = dummy;
	size++;
	return newSignal;
 }

	public static Signal FetchSignal(){
		Signal dummy;
		dummy = list.next;
		list.next = dummy.next;
		dummy.next = null;
		size--;
		return dummy;
	}
	public static int Size(){
		return size;
	}
}
