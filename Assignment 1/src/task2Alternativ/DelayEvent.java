package task2Alternativ;

import java.util.PriorityQueue;

/**
 * Created by Per on 29/05/2014.
 */
public class DelayEvent extends Event {
	private static final int CONST=0;
	private static final int EXP=1;
	private static int typ;
	private Buffer queue;
	public DelayEvent(double time,Buffer queue)
	{
		super(time);
		this.queue = queue;
	}

	public void treatEvent() {
		Consumer c = new Consumer(Consumer.B,time);
		queue.addBufferB(c);

		if(queue.size()==1){
			Simulation2.eventQueue.add(new ReadyEvent(time + ReadyEvent.getTime(Buffer.B),queue,Buffer.B));
		}
	}
	public static void SetCONST(){
		typ = CONST;
	}
	public static void setEXP(){
		typ = EXP;
	}
	public static double timeToAdd(){
		if(typ == CONST){
			return 1;
		}else{
			return Simulation2.getExponatial(1);
		}
	}

}
