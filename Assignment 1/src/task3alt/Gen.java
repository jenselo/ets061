package task3alt;

/**
 * Created by Per on 31/05/2014.
 */
public class Gen extends Proc {
	private double meanTime;
	public Gen (double meanTime){
		this.meanTime = meanTime;
	}

	public void treatSignal(Signal x) {
		switch (x.getType()){
			case Signal.READY:
				MainSimulation.signalList.add(new Signal(next,Signal.ARRIVAL,x.getArivallTime(),new Customer(x.getArivallTime())));
				MainSimulation.signalList.add(new Signal(this,Signal.READY,x.getArivallTime()+MainSimulation.getExponatial(meanTime),null));
				break;
		}


	}
}
