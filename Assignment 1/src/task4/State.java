package task4;

import java.io.IOException;
import java.util.Random;

class State extends GlobalSimulation{
	// Here follows the state variables and other variables that might be needed
	// e.g. for measurements
	public int noBusyServers = 0, accumulated = 0, noMeasurements = 0;
	public Random chance = new Random();

	// The following method is called by the main program each time a new event has been fetched
	// from the event list in the main loop. 
	public void treatEvent(Event x){
		switch (x.eventType){
			case ARRIVAL:
				arrival();
				break;
			case READY:
				ready();
				break;
			case MEASURE:
				measure();
				break;
		}
	}
	
	
	// The following methods defines what should be done when an event takes place. This could
	// have been placed in the case in treatEvent, but often it is simpler to write a method if 
	// things are getting more complicated than this.
	
	private void arrival(){
		if(noBusyServers < N) {
			double serviceTime = GlobalSimulation.x;
			insertEvent(READY, time + serviceTime);
			noBusyServers++;
		}

		double newArrivalTime = (-1.0/lambda)*Math.log(chance.nextDouble());
		insertEvent(ARRIVAL, time + newArrivalTime);
	}
	
	private void ready() {
		noBusyServers--;
	}

	private void measure(){
		//accumulated = accumulated + numberInQueue1 + numberInQueue2;

		try {
			String text = Integer.toString(noBusyServers) + '\n';
			fw.write(text);
		} catch(IOException e) {
			System.out.println("Couldn't write to file.");
			System.exit(1);
		}

		noMeasurements++;
		insertEvent(MEASURE, time + T);

	}
}