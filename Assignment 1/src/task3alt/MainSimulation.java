package task3alt;



import java.util.PriorityQueue;
import java.util.Random;

/**
 * Created by Per on 01/06/2014.
 */
public class MainSimulation extends Global {
	public static PriorityQueue<Signal>  signalList = new PriorityQueue<Signal>();
	public static final Random rand = new Random();
	public static void run(){
		Customer.reset();
		QS.reset();
		signalList = new PriorityQueue<Signal>();
		QS qs1 = new QS() ;
		QS qs2 = new QS();
		Gen gen = new Gen(1.1);
		simulater(qs1,qs2,gen);
		System.out.println("|---------------------------------------------------");
		System.out.println("|GENRATOR MEANTIME 1.1 \n |----------------------------------------------------");
		System.out.println("|Mean jobs in queue : "+QS.getMeanNumberInQueue());
		System.out.println("|Meantime in system: " + Customer.meanTimeInSystem() + "\n");
		Customer.reset();
		QS.reset();
		signalList = new PriorityQueue<Signal>();
		qs1 = new QS() ;
		qs2 = new QS();
		gen = new Gen(1.5);
		simulater(qs1,qs2,gen);
		System.out.println("|---------------------------------------------------");
		System.out.println("|GENRATOR MEANTIME 1.5 \n |----------------------------------------------------");
		System.out.println("|Mean jobs queue : "+QS.getMeanNumberInQueue());
		System.out.println("|Meantime in system: "+Customer.meanTimeInSystem()+"\n");
		Customer.reset();
		QS.reset();
		signalList = new PriorityQueue<Signal>();
		qs1 = new QS() ;
		qs2 = new QS();
		gen = new Gen(2.0);
		simulater(qs1,qs2,gen);
		System.out.println("|---------------------------------------------------");
		System.out.println("|GENRATOR MEANTIME 2.0 \n|---------------------------------------------------");
		System.out.println("|Mean jobs in queue : "+QS.getMeanNumberInQueue());
		System.out.println("|Meantime in system: "+Customer.meanTimeInSystem()+"\n");

	}
	public static void simulater(QS qs1,QS qs2,Gen gen){
		gen.setNextProc(qs1);
		qs1.setNextProc(qs2);
		signalList.add(new Signal(gen,Signal.READY,0,null));
		signalList.add(new Signal(qs1,Signal.MEASUREMENT,MainSimulation.getExponatial(5.0),null));
		while(QS.returnMeasur()<100000){
			signalList.poll().treatSignal();
		}

	}
	public static double getExponatial(double mean){
		return -(Math.log(rand.nextDouble()) / (1.0/mean));
	}

}
