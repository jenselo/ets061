package task2_rev.task2_rev;

import java.util.LinkedList;

public class EventListClass {
	private Event list, last; // Used to build a linked list

	private int size;
	private int prioEventType;
	private int notPrio;

	EventListClass(int prioEventType) {
		this();
		this.prioEventType = prioEventType;

		if(this.prioEventType == GlobalSimulation.READY_ARRIVAL) {
			this.notPrio = GlobalSimulation.READY_TEAR_DOWN;
		} else {
			this.notPrio = GlobalSimulation.READY_ARRIVAL;
		}
	}

	EventListClass(){
		list = new Event();
    	last = new Event();
    	list.next = last;
	}

	// The method insertEvent creates a new event, and searches the list of events for the
	// right place to put the new event.
	public void InsertEvent(int type, double TimeOfEvent){
 	    Event dummy, predummy;
		Event newEvent = new Event(type, TimeOfEvent);

 	    predummy = list;
 	    dummy = list.next;
 	    while ((dummy.eventTime < newEvent.eventTime) & (dummy != last)){
 		    predummy = dummy;
 		    dummy = dummy.next;
     	}
 	    predummy.next = newEvent;
 	    newEvent.next = dummy;
		size++;
    }

	public int getSize() {
		return size;
	}

	public void printList() {
		Event dummy = list.next;

		while(dummy != last) {
			System.out.println(dummy.getEventTime() + " ---------- " + dummy.getEventType());
			dummy = dummy.next;
		}
	}

	// The following method removes and returns the first event in the list. That is the
	// event with the smallest time stamp, i.e. the next thing that shall take place.

	// Fetch the prioritized event
	public Event fetchEvent() {
		Event retval = null;

		if(list.next.eventType == notPrio  && prioInList()) {
			retval = popNextPrio();
		} else {
			retval = popNext();
		}

		return retval;
	}

	private boolean prioInList() {
		Event dummy = list.next;

		while(dummy != last) {
			if(dummy.eventType == prioEventType) {
				return true;
			}
			dummy = dummy.next;
		}

		return false;
	}

	private Event popNextPrio() {
		Event dummy = list.next;

		while(dummy.next.eventType != prioEventType && dummy.next != last) {
			dummy = dummy.next;
		}

		Event retval = dummy.next;
		Event c = retval.next;
		retval.next = null;

		// Remove prioevent
		dummy.next = c;
		size--;

		return retval;
	}

	private Event popNext() {
		Event dummy;
		dummy = list.next;
		list.next = dummy.next;
		dummy.next = null;
		size--;
		return dummy;
	}
}