package task2;

import java.util.ArrayList;
import java.util.Random;

class Gen extends Proc {
	Random slump = new Random();

	public Proc sendTo;
	public double lambda;

	public void TreatSignal(Signal x){
		switch (x.signalType){
			case READY: {
				SignalList.SendSignal(ARRIVAL, sendTo, time);
				// if earlier than 17:00
				if (time < 480) {
					//SignalList.SendSignal(READY, this, time + Math.round(getPoisson(0.06667)));
					double meanArrivalTime = 4.0/60;
					double arrivalTime = getPoisson(meanArrivalTime);
					SignalList.SendSignal(READY, this, time + arrivalTime);
				}
				break;
			}
		}
	}
	private double getPoisson(double number){
		//return -number * Math.log(slump.nextDouble());
		return (-1/number)*Math.log(slump.nextDouble());
	}
}