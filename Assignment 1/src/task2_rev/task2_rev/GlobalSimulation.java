package task2_rev.task2_rev;

public class GlobalSimulation {
	
	// This class contains the definition of the events that shall take place in the
	// simulation. It also contains the global time, the event list and also a method
	// for insertion of events in the event list. That is just for making the code in
	// task1.Task4.java and task1.State.java simpler (no dot notation is needed).

	public static final int ARRIVAL = 1, READY_ARRIVAL = 2, TEARDOWN = 3, READY_TEAR_DOWN = 4, MEASURE = 5;
	public static final String[] EventTypes = {"ARRIVAL", "READY_ARRIVAL", "TEARDOWN", "READY_TEAR_DOWN", "MEASURE"};
	public static double time = 0; // The global time variable
	public static int discards = 0;
	public static double arrivalTime;
	public static EventListClass eventList = new EventListClass(); // The event list used in the program
	public static boolean expDelay;

	public static void insertEvent(int type, double TimeOfEvent){  // Just to be able to skip dot notation
		eventList.InsertEvent(type, TimeOfEvent);
	}
}