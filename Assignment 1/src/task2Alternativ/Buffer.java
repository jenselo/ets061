package task2Alternativ;

import java.util.PriorityQueue;

/**
 * Created by Per on 31/05/2014.
 */
public class Buffer
{
	public static final int TYPA = 0;
	public static final int TYPB = 1;
	public static final int A = 0;
	public static final int B = 1;
	private int prio;
	public static PriorityQueue<Consumer> bufferA;
	public static PriorityQueue<Consumer> bufferB;

	public Buffer(int prio){
		bufferA = new PriorityQueue<Consumer>();
		bufferB = new PriorityQueue<Consumer>();
		this.prio = prio;
	}

	public void addBufferA(Consumer c){
		bufferA.add(c);
	}
	public void addBufferB(Consumer c){
		bufferB.add(c);
	}
	public int size(){
		return bufferA.size()+bufferB.size();
	}

	public Consumer pull(int type){
		if(type == TYPA){
			return bufferA.poll();
		}else{
			return bufferB.poll();
		}
	}

	public int chooser(){
		if (prio==B){
			if(bufferB.size()>0){
				//System.out.println(bufferB.size());
				return TYPB;
			}else{
				return TYPA;
			}
		}else{
			if(bufferA.size()>0){
				return TYPA;
			}else{
				return TYPB;
			}
		}
	}
}
