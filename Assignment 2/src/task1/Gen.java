package task1;

import java.util.*;

class Gen extends Proc{
	Random slump = new Random();

	public Proc sendTo;
	public double lambda;
	private ArrayList<QS> QSS;
	int i = -1;
	int current = -1;
	int procQueueType = -1;
	double meanArrivalTime;

	public Gen(ArrayList<QS> QSS, int queueType, double meanArrivalTime){
		this.QSS = QSS;
		this.procQueueType = queueType;
		this.meanArrivalTime = meanArrivalTime;
	}

	public void TreatSignal(Signal x){
		switch (x.signalType){
			case READY:
				i = getQueue();
				sendTo  = QSS.get(i);
				SignalList.SendSignal(ARRIVAL, sendTo, time);
				double meanServiceTime = 0.5;

				double arrivalTime = slump.nextDouble() * meanServiceTime * 2;
				SignalList.SendSignal(READY, this, time + arrivalTime);
				break;
		}
	}
	private int Test(){
		QS min = null ;
		ArrayList<QS> test = new ArrayList<QS>();
		for (QS i : QSS){
			if (min == null){
				min = i;
			}
			if(i.numberInQueue<min.numberInQueue){
				min = i;
			}
		}
		for (QS i : QSS){
			if(i.numberInQueue == min.numberInQueue){
				test.add(i);
			}
		}
		min = test.get(slump.nextInt(test.size()));
		return min.name;

	}

	public int getQueue() {
		switch(procQueueType) {
			case RANDOM:{
				Random rand = new Random();
				current = rand.nextInt(QSS.size());}
				break;
			case ROUNDROBIN:{
				current = (current+1)%QSS.size();}
				break;
			case SMART:{
					// Find smallest
					int min = Integer.MAX_VALUE;
					for(int j = 0; j < QSS.size(); j++) {
						QS temp = QSS.get(j);

						if (temp.getSize() < min) {
							min = temp.getSize();
							current = j;
						}
					}

					ArrayList<Integer> possQueue = new ArrayList<Integer>();
					possQueue.add(current);
					// Find with equal size
					for(int j = 0; j < QSS.size(); j++) {
						QS temp = QSS.get(j);

						if(current != j && temp.getSize() == min) {
							possQueue.add(j);
						}
					}

					Random rand = new Random();
					current = possQueue.get(rand.nextInt(possQueue.size()));

				}
				break;
			default:
				break;
		}

		return current;
	}
}