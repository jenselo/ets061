package task2;

import java.util.Random;

class State extends GlobalSimulation{
	public int numberInQueueA = 0, numberInQueueB = 0, accumulated = 0, noMeasurements = 0;

	Random chance = new Random();
	
	// The following method is called by the main program each time a new event has been fetched
	// from the event list in the main loop. 
	public void treatEvent(Event x){
		switch (x.eventType){
			case ARRIVALQA:
				arrivalQA();
				break;
			case READYQA:
				readyQA();
				break;
            case ARRIVALQB:
                arrivalQB();
				break;
            case READYQB:
                readyQB();
				break;
			case MEASURE:
				measure();
				break;
		}
	}
	
	// The following methods defines what should be done when an event takes place. This could
	// have been placed in the case in treatEvent, but often it is simpler to write a method if 
	// things are getting more complicated than this.
	private void arrivalQA(){
		double Xa = 0.002;
		if(numberInQueueA == 0) {
			insertEvent(READYQA, time + Xa);
			numberInQueueA++;
		} else {
			numberInQueueA++;
		}

		double arrivalTime = (-1/150.0)*Math.log(chance.nextDouble());
		insertEvent(ARRIVALQA, time+arrivalTime);
	}

    private void arrivalQB() {
		if(numberInQueueB == 0) {
			double Xb = 0.004;
			insertEvent(READYQB, time+Xb);
			numberInQueueB++;
		} else {
			numberInQueueB++;
		}
    }
	
	private void readyQA() {
		double d = 0;
		if(expDelay) {
			d = -(Math.log(chance.nextDouble()) / (1.0));
		} else {
			d = 1;
		}

		insertEvent(ARRIVALQB, time + (double) d);
		numberInQueueA--;
	}

    private void readyQB() {
		numberInQueueB--;
    }
	
	private void measure(){
		accumulated += numberInQueueB + numberInQueueA;
		noMeasurements++;
		insertEvent(MEASURE, time+0.1);
	}
}