package task1alt;

/**
 * Created by Per on 29/05/2014.
 */
public class Measurements extends Event {
	private static double measurement ;
	private static double totalInQueue ;
	public Measurements(double time){
		super(time);
	}


	public void treatEvent() {
		measurement +=1;
		//System.out.println(Simulation.q2.size());
		totalInQueue += Simulation.q2.size();
		Simulation.eventQueue.add(new Measurements(time + -(Math.log(Simulation.rand.nextDouble()) / (1/5.0))));
	}
	public static void reset(){
			measurement =0;
			totalInQueue =0;
	}
	public static double getMeasurement(){
		return measurement;
	}
	public static double getTotalInQueue(){
		return totalInQueue;
	}
}
