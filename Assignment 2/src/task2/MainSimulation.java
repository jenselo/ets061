package task2;

public class MainSimulation extends Global {

	public static void run() {
		double accumulatedTime = 0;
	    Signal actSignal;
	    SignalList signaList = new SignalList();

	    QS prescriptionQueue = new QS(1);
	    Gen Generator = new Gen();
	    Generator.lambda = 9;
	    Generator.sendTo = prescriptionQueue;

		int days = 0;

	    while (days < 10000) {
			prescriptionQueue.resetTimeList();
		    time = 0;
			signaList = new SignalList();
		    SignalList.SendSignal(READY, Generator, time);
		    treatDay();
		    days++;
		    accumulatedTime += time;
	    }

		double meanServiceTime = prescriptionQueue.totalTimeRecipes/prescriptionQueue.numberOfRecipes;
		double meanQueueTime = prescriptionQueue.totalQueueTime/prescriptionQueue.numberOfRecipes;

	    System.out.println("Mean servicetime: " + meanServiceTime);
		System.out.println("Mean queuetime: " + meanQueueTime);
		System.out.println("Mean time in system: " + (meanQueueTime+meanServiceTime));
	    System.out.println("Mean endtime: "+ (accumulatedTime/(60*days)+9));
    }

	public static void treatDay(){
		Signal actsignal;
		while (SignalList.Size()>0) {
			actsignal = SignalList.FetchSignal();
			time = actsignal.arrivalTime;
			actsignal.destination.TreatSignal(actsignal);
		}
	}
}