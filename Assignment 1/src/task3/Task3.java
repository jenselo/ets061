package task3;

import java.io.*;
import java.util.Random;

public class Task3 extends GlobalSimulation {
	public static void run() {

		double[] arrivalTimes = {2.0, 1.5, 1.1};
		double[] meanCusts = new double[3];
		double[] meanTimes = new double[3];
		int[] custs = new int[3];
		int[] custsNoAcc = new int[3];
		Random rand = new Random();

		for (int i = 0; i < arrivalTimes.length; i++) {
			double arrivalTime = arrivalTimes[i];

			Event actEvent;
			State actState = new State(arrivalTime); // The state that should be used

			// Some events must be put in the event list at the beginning
			insertEvent(ARRIVALQ1, 0);
			insertEvent(MEASURE, -(Math.log(rand.nextDouble()) / (1/5.0)));

			// The main simulation loop
			while (actState.noMeasurements < 1000) {
				actEvent = eventList.fetchEvent();
				time = actEvent.eventTime;
				actState.treatEvent(actEvent);
			}

			meanTimes[i] = actState.getMeanTimeSpent();
			meanCusts[i] = actState.getMeanCustomers();
			custs[i] = actState.getCustomers();
			custsNoAcc[i] = actState.getCustomersNonAcc();
		}

		System.out.println();
		for (int i = 0; i < arrivalTimes.length; i++) {
			System.out.println("Mean no. customers (" + arrivalTimes[i] + "): " + meanCusts[i]);
			//System.out.println("Total no. customers (" + arrivalTimes[i] + "): " + custs[i]);
			//System.out.println("Total no. customers (no acc) (" + arrivalTimes[i] + "): " + custsNoAcc[i]);
			System.out.println("Mean time spent in system (" + arrivalTimes[i] + "): " + meanTimes[i]);
			System.out.println();
		}
	}
}