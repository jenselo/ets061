package task1alt;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Per on 29/05/2014.
 */
public abstract class EventReady extends Event{
	protected ArrayList<Queuer> queue;
	public EventReady(double time, ArrayList<Queuer> queue){
		super(time);
		this.queue = queue;
	}
	abstract public void treatEvent() ;

	abstract protected void decreaseQueue();
}
