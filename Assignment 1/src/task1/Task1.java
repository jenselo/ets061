package task1;

import java.util.Random;

public class Task1 extends GlobalSimulation {

//public static void main(String[] args) throws IOException {
	public static void run() {
		double meanSize[] = {0, 0, 0};
		double noDiscards[] = {0, 0, 0};
		int totalNoCusts[] = {0, 0, 0};

		for (int i = 0; i < meanSize.length; i++) {
			double meanInternal = 0;
			arrivalTime = ARRIVAL_TIMES[i];

			// Reset simulation variables
			discards = 0;
			Event actEvent;
			State actState = new State(); // The state that should be used
			time = 0;

			Random chance = new Random();
			// Some events must be put in the event list at the beginning
			insertEvent(ARRIVALQ1, 0);
			insertEvent(MEASURE, -(Math.log(chance.nextDouble()) / (1/5.0)));

			// The main simulation loop
			while (actState.noMeasurements < 1000) {
				actEvent = eventList.fetchEvent();
				time = actEvent.eventTime;
				actState.treatEvent(actEvent);
			}

			meanSize[i] = actState.accumulatedQ2 / (double) actState.noMeasurements;
			noDiscards[i] = discards;
			totalNoCusts[i] = actState.accumulatedQ1;
		}

		System.out.println();
		System.out.println("1: Mean no. customers Q2: " + meanSize[0] + ". Discards: " + noDiscards[0] + ". Disc prob:" + noDiscards[0] / (noDiscards[0] + totalNoCusts[0]));
		System.out.println("2: Mean no. customers Q2: " + meanSize[1] + ". Discards: " + noDiscards[1] + ". Disc prob:" + noDiscards[1] / (noDiscards[1] + totalNoCusts[1]));
		System.out.println("5: Mean no. customers Q2: " + meanSize[2] + ". Discards: " + noDiscards[2] + ". Disc prob:" + noDiscards[2] / (noDiscards[2] + totalNoCusts[2]));
		// Printing the result of the simulation, in this case a mean value
		//System.out.println(1.0*actState.accumulated/actState.noMeasurements);
	}
}
