package task1alt;

import java.util.ArrayList;

/**
 * Created by Per on 29/05/2014.
 */
public class EventQueue1 extends EventQueue {
	private static int rejects;
	private static int totalNumber;

	public EventQueue1(double time,ArrayList<Queuer> queue){
		super(time,queue);
	}

	public void treatEvent() {
		double nextTime;
		if(queue.size()==0){
			// Increment queue
			increaseQueue();

			nextTime = -(Math.log(Simulation.rand.nextDouble())*2.1);
			Simulation.eventQueue.add(new EventReady1(time + nextTime, Simulation.q1));
		} else if(queue.size() >= 1 && queue.size()<11){
			increaseQueue();
			//System.out.println("Queued");
		} else if (queue.size() >= 11)
		{
			//System.out.println("Rejected");
			rejects++;
		}
		nextTime = Simulation.arriveTime1;
		totalNumber++;
		Simulation.eventQueue.add(new EventQueue1(time + nextTime,Simulation.q1));

	}

	public static void reset(){
		rejects = 0;
		totalNumber = 0;
	}

	public static double chanceOfRejection(){
		return rejects /((double)totalNumber);
	}

	protected void increaseQueue() {
		queue.add(new Queuer());
	}
}

