package task2_rev.task2_rev;

import java.util.Random;

class State extends GlobalSimulation{
	public int numberInQueue = 0, accumulated = 0, noMeasurements = 0;

	Random chance = new Random();
	
	// The following method is called by the main program each time a new event has been fetched
	// from the event list in the main loop. 
	public void treatEvent(Event x) {
		//System.out.println(time + ": " + GlobalSimulation.EventTypes[x.eventType-1]);
		switch (x.eventType){
			case ARRIVAL:
				arrival();
				break;
			case READY_ARRIVAL:
				ready_arrival();
				break;
            case TEARDOWN:
                teardown();
				break;
			case READY_TEAR_DOWN:
				ready_teardown();
				break;
			case MEASURE:
				measure();
				break;
		}
	}

	// The following methods defines what should be done when an event takes place. This could
	// have been placed in the case in treatEvent, but often it is simpler to write a method if
	// things are getting more complicated than this.

	private void arrival() {
		if(numberInQueue == 0) {
			double Xa = 0.002;
			insertEvent(READY_ARRIVAL, time + Xa);
		}

		numberInQueue++;

		//double nextArrival = (-1/150.0)*Math.log(chance.nextDouble());
		//insertEvent(ARRIVAL, time + nextArrival);
	}

	private void ready_arrival() {
		double d = calc_delay();
		delay_to(d);
		numberInQueue--;
	}

	private void teardown() {
		if(numberInQueue == 0) {
			double Xb = 0.004;
			insertEvent(READY_TEAR_DOWN, time + Xb);
		}

		numberInQueue++;
	}

	private void ready_teardown() {
		numberInQueue--;
		double nextArrival = (-1/150.0)*Math.log(chance.nextDouble());
		insertEvent(ARRIVAL, time + nextArrival);
	}

	private void delay_to(double dt) {
		insertEvent(TEARDOWN, time + dt);
	}

	private double calc_delay() {
		return 1.0;
	}

	private void measure(){
		accumulated += numberInQueue;
		noMeasurements++;
		insertEvent(MEASURE, time+0.1);
	}
}