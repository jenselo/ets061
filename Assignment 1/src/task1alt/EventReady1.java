package task1alt;

import java.util.ArrayList;

/**
 * Created by Per on 29/05/2014.
 */
public class EventReady1 extends EventReady {
	public EventReady1(double time,ArrayList<Queuer> queue){
		super(time,queue);
	}

	public void treatEvent() {
		double newtime;
		Simulation.eventQueue.add(new EventQueue2(time,Simulation.q2));
		decreaseQueue();

		if(queue.size()>0){
			newtime = -(Math.log(Simulation.rand.nextDouble()) / (1/2.1));
			Simulation.eventQueue.add(new EventReady1(time + newtime,queue));

		}
	}

	protected void decreaseQueue() {
		if(queue.size() > 0) {
			queue.remove(queue.size() - 1);
		}
	}
}
