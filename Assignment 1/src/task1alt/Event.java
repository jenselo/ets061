package task1alt;

public abstract class Event implements Comparable<Event>{
	protected double time;
	//static double totalTime = 0;
	public Event(double time ){
		this.time = time;
	}
	//public void updateTotalTime(double updatedTime){totalTime = updatedTime;}
	public double getTime(){
		return time;
	}
	//public void resetTotalTime(){
		//totalTime = 0;
	//}

	public abstract void treatEvent();

	@Override
	public int compareTo(Event o) {
		return new Double(time).compareTo(new Double(o.time));
	}
}