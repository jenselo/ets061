package task2_rev.task2_rev;

public class Task2_rev extends GlobalSimulation {

	public static void run() {
		int k = 0;
		boolean[] expDel = {false, true};
		double[] res = new double[2];

		boolean b = expDel[0];

		eventList = new EventListClass(READY_TEAR_DOWN);

		// Reset simulation variables
		discards = 0;
		Event actEvent;
		State actState = new State(); // The state that should be used
		time = 0;

		// Some events must be put in the event list at the beginning
		insertEvent(ARRIVAL, 0);
		insertEvent(MEASURE, 0.1);

		boolean printed = false;
		// The main simulation loop
		while (actState.noMeasurements < 1000) {
			actEvent = eventList.fetchEvent();
			time = actEvent.eventTime;
			actState.treatEvent(actEvent);
		}

		double meanNoCustomers = actState.accumulated / (double) actState.noMeasurements;
		res[k] = meanNoCustomers;
		k++;


		System.out.println();
		System.out.println("Mean number of customers with prio A: " + res[0]);
	}
}
