package task3alt;

/**
 * Created by Per on 31/05/2014.
 */
public class Signal implements Comparable<Signal>{
	public static final int ARRIVAL = 0;
	public static final int READY = 1;
	public static final int MEASUREMENT = 2;
	private Proc Dest;
	private int type;
	private double arivallTime;
	private Customer c;

	public Signal (Proc Dest,int type,double time,Customer c){
		this.Dest = Dest;
		this.type = type;
		this.arivallTime = time;
		this.c = c;
	}

	public double getArivallTime(){
		return arivallTime;
	}

	public void treatSignal(){
		Dest.treatSignal(this);
	}
	public int getType(){
		return type;
	}

	public int compareTo(Signal o) {
		return Double.compare(arivallTime,o.arivallTime);
	}
	public Customer getCustomer(){return c;}



}
